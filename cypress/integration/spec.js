describe('Coveo Search Tests', function() {
  it('Clicking Card Button Should Show Results In Card View', function() {

    cy.visit("https://www.dell.com/koa/search/?open=michael")
    //.then(()=> {

    cy.wait(20000)

    cy.get("#toggle-button").click()    
    
    //})
  })
})

describe('Unified Dell Search', function() {
  it('Clicking Card Button Should Show Results In Card View', function() {
    
    cy.visit('http://pilot.search.dell.com/laptops/xps#products')

    cy.get('#list').click()

    cy.get('#grid').click()
  })
})

describe('Cypress Sample E2E Tests', () => {
  it('clicking "type" shows the right headings', function() {

    cy.visit('https://example.cypress.io')

    cy.pause()

    cy.contains('type').click()

    // Should be on a new URL which includes '/commands/actions'
    cy.url().should('include', '/commands/actions')

    // Get an input, type into it and verify that the value has been updated
    cy.get('.action-email')
      .type('fake@email.com')
      .should('have.value', 'fake@email.com')
  })
})

